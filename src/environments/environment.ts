// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCbdEkIR9p0wtLLbfNFJ_nQRAzyOkRtnRY",
    authDomain: "test2-tamar.firebaseapp.com",
    projectId: "test2-tamar",
    storageBucket: "test2-tamar.appspot.com",
    messagingSenderId: "953570782833",
    appId: "1:953570782833:web:d1a92576dd922a28fbb5d0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
