export interface Item {
    id: string,
    first:string,
    second:number,
    third:number,
    image:string,
    saved?:Boolean, //האם הערך נשמר בפיירבייס או לא
    result?:string //תוצאת החיזוי
}
