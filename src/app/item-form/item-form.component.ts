import { PredictionService } from './../prediction.service';
import { ItemService } from './../item.service';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Item } from '../interfaces/item';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {

  userId;
  first;
  second;
  third;
  saved;
  result;
  button=true;
  selected;
  
  constructor(public authService:AuthService, private router:Router, private itemService:ItemService, private PredictionService:PredictionService) { }

  cancel(){
    this.button = true;
    this.result = null;
  }
  
  save(item:Item){
    this.itemService.addItem(this.userId, this.first,this.second, this.third, this.result); 
    this.router.navigate(['/items']); 
  }

  predict(){ //
    this.result = 'Load';
    this.button = false;
    this.PredictionService.predict(this.second, this.third).subscribe( 
      res => {
        console.log(res);
        if(res = "False"){ 
          var result = "Not available in stock";
          this.selected = "Not available in stock"
        } else{ 
          var result = "Available in stock";
          this.selected = "Available in stock"
        }
        this.result = result
        }
    );  
  }
  
  ngOnInit(): void {
    {
      this.authService.getUser().subscribe(
        user => {
        this.userId = user.uid;
        console.log(this.userId)
        })
    }
  }

}
