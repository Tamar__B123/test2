import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Item } from './interfaces/item';
import { Weather } from './interfaces/weather';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  deleteItem(userId:string, id:string){
    this.db.doc(`users/${userId}/elements/${id}`).delete();
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/elements/${id}`).update(
      {
        result:result
      })
    }
  
  updateItem(userId:string, id:string,item:Item, result: string){
    this.db.doc(`users/${userId}/elements/${id}`).update(
      {
        second: item.second,
        third: item.third,
        image: item.image,
        result: result,
        saved:true
      })
  }

  async clearItem(userId:string, id:string){
    await this.db.doc(`users/${userId}/elements/${id}`).update(
      {
        second: '',
        third: '',
        image: '',
        result: '',
        saved:false
      })
  }

  addItem(userId:string, first:string, second:string, third:string, result:string){ //מה שצריך כדי לעדכן את הדאטה בייס זה מי היוזר שמוסיף פריט, ומה הם השדות שלו ר 
    const item = {first:first, second:second, third:third, result:result}; //נגדיר משתנה קבוע של פריט
    this.userCollection.doc(userId).collection('elements').add(item); //פשוט עוד דרך במקום לכתוב במקום את הנתיב המלא כמו במחיקה ובעדכון,כמו שבנוי הדאטה בייס: פונים לקולקיישן יוזרס, ואז לדוקומנט יוזר איידי, ואז לקולקיישן אייטמס, ומוסיפים לשם את האייטם
  }

  itemCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  /*updateItem(userId:string,id:string,first:string,second:string, third:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        first:first,
        second:second,
        third:third
      }
    )
  }*/

  public getItems(userId){ //הפונקציה בונה רפרנס לאייטמס קולקשין 
    this.itemCollection = this.db.collection(`users/${userId}/elements`);//לפונקציה קולקשין נכניס את הנתיב של האייטמס
    return this.itemCollection.snapshotChanges()
    
    }

  constructor(private db:AngularFirestore, private authService:AuthService) { }
}
