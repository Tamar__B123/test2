import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Item } from '../interfaces/item';
import { ItemService } from '../item.service';
import { WeatherapiService } from '../weatherapi.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items:Item[];
  items$;
  displayedColumns: string[] = ['City', 'Temp', 'Humidity', 'icon', 'result', 'Actions'];
  userId;
  showdelete:boolean = true;
  areYouSure:boolean = false;

  /*update(item:Item){
    this.itemService.updateItem(this.userId, item.id, item.first, item.second, item.third);
    this.router.navigate(['/item-form']); 
  }*/

  getWeatherData(index){
    this.weatherService.searchWeatherData(this.items[index].first).subscribe(
      data => {
        console.log(data)
        this.items[index].second = data.temperature
        this.items[index].third = data.humidity
        this.items[index].image = data.image
      }
    )
  }

  predict(index){ //
    this.items[index].result = '';
    // this.button = false;
    if(this.items[index].second && this.items[index].first){
      this.predictionService.predict(this.items[index].second, this.items[index].third).subscribe( 
        res => {
          console.log(res);
          if(res > 0.5){ 
            var result = "Going to rain";
            // this.selected = "Not available in stock"
          } else{ 
            var result = "Not going to rain";
            // this.selected = "Available in stock"
          }
          this.items[index].result = result
          }
      );
    }
      
  }

  async getNewData(index){
    await this.itemService.clearItem(this.userId,this.items[index].id);
    this.getWeatherData(index)
  }

  saveItem(index){
    this.itemService.updateItem(this.userId,this.items[index].id, this.items[index], this.items[index].result);
  }

  deleteItem(index){
    this.itemService.deleteItem(this.userId,this.items[index].id); 
  }
  
  constructor(private weatherService: WeatherapiService,private db:AngularFireDatabase, public authService:AuthService, private itemService:ItemService, private predictionService:PredictionService,  private router:Router) { }

  show(){
    if(!this.areYouSure)
    this.showdelete = true;
  }

  hide(){
    this.showdelete = false;
  }  

  showAreYouSure(){
     this.areYouSure = true;
     this.showdelete = false;
  }

  hideAreYouSure(){
    this.areYouSure = false;
    this.showdelete = true;
  } 
  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.items$ = this.itemService.getItems(this.userId);
        this.items$.subscribe(
          docs =>{
            console.log('init worked');             
            this.items = [];
            for(let document of docs){
              const item:Item = document.payload.doc.data();
              item.id = document.payload.doc.id; 
              this.items.push(item); 
            }
          }
        ) 
      }
    )
  }

}
