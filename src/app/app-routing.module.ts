import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { ItemFormComponent } from './item-form/item-form.component';
import { ItemsComponent } from './items/items.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'bye', component: ByeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: SignUpComponent},  
  { path: 'weather', component: ItemsComponent},
  { path: 'item-form', component: ItemFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
